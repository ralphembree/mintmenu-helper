mintmenu-helper
===============

Improve the text box in MintMenu by performing sensible actions based on the input.

## Features
* File names and folder names, regardless of spaces, are opened from the search box.
* If the first word is the name of an executable it will be executed with the remaining text as arguments.
* Even terminal applications will run and the output remains until CTRL-C is hit.
* When a search is performed arguments to mate-search-tool are passed on.

## Installation

### Graphical
1. Download the package (click `Download ZIP` above).
1. Unzip `master.zip`.
1. Move the mintmenu-helper file (not the whole folder) somewhere in your `$PATH` such as `/usr/bin/`.
1. Remove the mintmenu-helper folder.
1. Right click your menu (probably bottom left corner of your screen), select "Preferences", switch to the "Applications" tab, select the "Search command" text box.
1. Type `mintmenu-helper %s` in the box.
1. Close.

### Command Line
```
git clone https://github.com/ralphembree/mintmenu-helper.git
sudo mv mintmenu-helper/mintmenu-helper /usr/bin/
gsettings set com.linuxmint.mintmenu.plugins.applications search-command "mintmenu-helper %s"
rm -rf mintmenu-helper
```
